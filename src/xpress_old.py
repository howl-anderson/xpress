#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import urlparse
import markdown
from jinja2 import Template
import BaseHTTPServer
import os
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
#TODO
from pyquery import PyQuery
from lxml import etree

xpress_index = "index"

current_dir = os.path.dirname(os.path.abspath(__file__))
www_path = os.path.join(current_dir, 'www')
ui_path = os.path.join(current_dir, 'ui')
tpl_path = os.path.join(current_dir, 'tpl')

rc_path = os.path.join(www_path, '.xpressrc')
config_fd = open(rc_path, 'r')
# NOTICE:only support utf8
config_stream = config_fd.read().decode("utf8")
config_fd.close()
config_info = load(config_stream, Loader=Loader)


class View:
    def __init__(self, path):
        self.page_size = 5
        self.path = path
        self.model = Model()
        self.xpress_url = urlparse.urlparse(self.path)
        self.xpress_url_params = urlparse.parse_qs(self.xpress_url.query)
        self.xpress_request_path = os.path.join(www_path, self.xpress_url.path[1:])

    def index(self, content_list):
        page_num = int((len(content_list) + self.page_size - 1) / self.page_size)
        if 'page' not in self.xpress_url_params:
            page_param = 0
        else:
            page_param = int(self.xpress_url_params["page"][0])
        if page_param < page_num - 1:
            self.page_start_point = page_param * self.page_size
            self.page_end_point = (page_param + 1) * self.page_size - 1
        elif page_param == page_num - 1:
            self.page_start_point = page_param * self.page_size
            self.page_end_point = len(content_list) - 1
        else:
            #TODO
            #print "hacker"
            return
        if page_param == 0:
            self.pre_page_flag = 0
            self.pre_page = 0
        else:
            self.pre_page_flag = 1
            self.pre_page = page_param - 1
        if page_param == page_num - 1:
            self.next_page_flag = 0
            self.next_page = 0
        else:
            self.next_page_flag = 1
            self.next_page = page_param + 1

        page_content = self.index_page(content_list)
        return page_content

    def index_page(self, file_list):
        if self.page_start_point >= len(file_list) or self.page_end_point >= len(file_list):
            #TODO Exception
            #start 
            print "something happend"
            return
        index_info = []
        for file_index in range(self.page_start_point, self.page_end_point + 1):
            file = file_list[file_index]
            article_file = os.path.join(www_path, file)

            meta_info, option_info, article_title, article_content, file_ctime = self.read_article(article_file)
            url = "/" + file
            try:
                pyquery_obj = PyQuery(article_content)
                summary_text = pyquery_obj("p:first").html()
                #in case there is no <p>
                if summary_text is None:
                    summary_text = ""
            except Exception,e:
                # TODO: pyquery sometime crash, need do something
                summary_text = ""
            article_summary = "<p>" + summary_text + "</p>"
            article_info = {"url": url,
                            "meta": meta_info,
                            "option": option_info,
                            "article": article_content,
                            "title": article_title,
                            "summary": article_summary,
                            "ctime": file_ctime}
            index_info.append(article_info)

        fd = open(os.path.join(tpl_path, xpress_index + '.html'), 'r')
        tpl = fd.read().decode("utf8")
        fd.close()
        index_hero_article = os.path.join(www_path, xpress_index)
        _, _, _, index_hero_unit, _ = self.read_article(index_hero_article)
        config_info = load(open(os.path.join(www_path, '.xpressrc'), 'r').read().decode("utf8"), Loader=Loader)

        template = Template(tpl)
        html = template.render(config=config_info, index=index_info, hero_unit=index_hero_unit, pre_page_flag=self.pre_page_flag, pre_page=self.pre_page, next_page_flag=self.next_page_flag, next_page=self.next_page)
        return html

    def category(self, content_list, category_path):
        page_num = int((len(content_list) + self.page_size - 1) / self.page_size)
        if 'page' not in self.xpress_url_params:
            page_param = 0
        else:
            page_param = int(self.xpress_url_params["page"][0])
        if page_param < page_num - 1:
            self.page_start_point = page_param * self.page_size
            self.page_end_point = (page_param + 1) * self.page_size - 1
        elif page_param == page_num - 1:
            self.page_start_point = page_param * self.page_size
            self.page_end_point = len(content_list) - 1
        else:
            #TODO
            #print "hacker"
            return
        if page_param == 0:
            self.pre_page_flag = 0
            self.pre_page = 0
        else:
            self.pre_page_flag = 1
            self.pre_page = page_param - 1
        if page_param == page_num - 1:
            self.next_page_flag = 0
            self.next_page = 0
        else:
            self.next_page_flag = 1
            self.next_page = page_param + 1

        page_content = self.category_page(content_list, category_path)
        return page_content

    def category_page(self, file_list, category_path):
        if self.page_start_point >= len(file_list) or self.page_end_point >= len(file_list):
            #start 
            print "something happend"
            return
            #TODO Exception
        index_info = []
        for file_index in range(self.page_start_point, self.page_end_point + 1):
            file = file_list[file_index]
            article_file = os.path.join(category_path, file)

            meta_info, option_info, article_title, article_content, file_ctime = self.read_article(article_file)
            url = '/' + os.path.relpath(article_file, www_path)
            try:
                pyquery_obj = PyQuery(article_content)
                summary_text = pyquery_obj("p:first").html()
                #in case there is no <p>
                if summary_text == None:
                    summary_text = ""
            except Exception,e:
                #TODO:pyquery sometime crash, need do something
                summary_text = ""
            article_summary = "<p>" + summary_text + "</p>"
            article_info = {"url": url,
                            "meta": meta_info,
                            "option": option_info,
                            "article": article_content,
                            "title": article_title,
                            "summary": article_summary,
                            "ctime": file_ctime}
            index_info.append(article_info)

        fd = open(os.path.join(tpl_path, 'category.html'), 'r')
        tpl = fd.read().decode("utf8")
        fd.close()
        index_hero_article = os.path.join(category_path, xpress_index)
        _, _, _, index_hero_unit, _ = self.read_article(index_hero_article)
        fd = open(os.path.join(www_path, '.xpressrc'), 'r')
        config_info = load(fd.read().decode("utf8"), Loader=Loader)
        fd.close()

        template = Template(tpl)
        html = template.render(config=config_info, index=index_info, hero_unit=index_hero_unit, pre_page_flag=self.pre_page_flag, pre_page=self.pre_page, next_page_flag=self.next_page_flag, next_page=self.next_page)
        return html

    def read_article(self, article_path):
        print article_path
        if os.path.isfile(article_path):
            file_stat = os.stat(article_path)
            file_ctime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_stat.st_ctime))

            fd = open(article_path, 'r')
            #content = fd.read().decode("utf-8", errors="ignore")
            content = fd.read().decode("utf8")
            fd.close()
            content_unit = content.split('\n\n', 3)
            meta = load(content_unit[0], Loader=Loader)
            option = load(content_unit[1], Loader=Loader)
            article_title = content_unit[2]
            article_content = markdown.markdown(content_unit[3])
            return meta, option, article_title, article_content, file_ctime
        else:
            return "", "", "", "", ""

    def page(self, request_path):
        article_file = request_path
        meta_info, option_info, article_title, article_content, file_ctime = self.read_article(article_file)
        fd = open(os.path.join(tpl_path, 'content.html'), 'r')
        tpl = fd.read().decode("utf8")
        fd.close()
        template = Template(tpl)
        html = template.render(config=config_info, article_content=article_content, option=option_info, article_title=article_title, ctime=file_ctime)
        return html


class Controller:
    def __init__(self, server):
        self.server = server
        self.model = Model()
        self.view = View(self.server.path)

    def index(self):
        file_list = self.model.index()
        html = self.view.index(file_list)

        self.server.send_response(200)
        self.server.send_header('Content-type', 'text/html')
        self.server.end_headers()
        self.server.wfile.write(html.encode('utf8', 'ignore'))
 
    def category(self, path):
        file_list = self.model.category(path)
        html = self.view.category(file_list, path)

        self.server.send_response(200)
        self.server.send_header('Content-type', 'text/html')
        self.server.end_headers()
        self.server.wfile.write(html.encode('utf8', 'ignore'))
 
    def page(self, path):
        file = self.model.page(path)
        if file:
            html = self.view.page(file)
            self.server.send_response(200)
            self.server.send_header('Content-type', 'text/html')
            self.server.end_headers()
            self.server.wfile.write(html.encode('utf8', 'ignore'))
        else:
            self.server.send_error(404)

class Model:
    def __init__(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        self.www_path = os.path.join(current_dir, 'www')

    def index(self):
        file_list = self.scan_dir(self.www_path)
        file_sorted_list = self.sort_file_by_ctime(file_list)
        relpath_list = []
        for file in file_sorted_list:
            file_relpath = os.path.relpath(file, www_path)
            #exclude the index page at the www root
            if file_relpath != xpress_index:
                relpath_list.append(file_relpath)
        return relpath_list

    def category(self, category_dir):
        file_list = self.scan_dir(category_dir)
        file_sorted_list = self.sort_file_by_ctime(file_list)
        relpath_list = []
        for file in file_sorted_list:
            file_relpath = os.path.relpath(file, category_dir)
            relpath_list.append(file_relpath)
        return relpath_list

    def scan_dir(self, dir):
        result = []
        yid = os.walk(dir)
        for rootDir, pathList, fileList in yid:
            for file in fileList:
                #filter the vi and emacs swap file,
                #other editor? sorry, we only support vi or emacs
                if self.filter_path(rootDir) and self.filter_path(file):
                    result.append(os.path.join(rootDir, file))
        return result

    def sort_file_by_ctime(self, file_list, sort_reverse=True):
        file_dict = {}
        for file in file_list:
            file_stat = os.stat(file)
            file_dict[file] = file_stat.st_ctime
        file_sorted_list = sorted(file_dict, key=file_dict.__getitem__,
                                  reverse=sort_reverse)
        return file_sorted_list

    def filter_path(self, path):
        """ filter the path if it is not so good """
        #print path
        #basepath = os.path.basename(path)
        #print basepath
        #if basepath.startswith(".") or basepath.startswith("#"):
        #    return False
        #else:
        #    return True

        #TODO for more secure
        print path
        path_list = path.split("/")
        for basepath in path_list:
            print basepath
            if basepath.startswith(".") or basepath.startswith("#"):
                return False
        return True

    def page(self, request_path):
        if not self.filter_path(request_path):
            return False
        else:
            return request_path

class WebRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init(self, request, client_address, server):
        super(WebRequestHandler, self).__init__(request, client_address, server)
        #BaseHTTPServer.BaseHTTPRequestHandler(request, client_address, server)

    def handle_one_request(self):
        print self.headers["Host"]
        super(WebRequestHandler, self).handle_one_request()

    def do_GET(self):
        self.xpress_url = urlparse.urlparse(self.path)
        self.xpress_url_params = urlparse.parse_qs(self.xpress_url.query)
        self.xpress_request_path = os.path.join(www_path, self.xpress_url.path[1:])
        control = Controller(self)
        if self.xpress_url.path == '/' or self.xpress_url.path == '/index':
            #if the request page is the index page
            #index_file_list = xpress.get_index(www_path)
            control.index()

        elif os.path.isfile(self.xpress_request_path):
            normal_path = os.path.normpath(self.xpress_request_path)
            if normal_path.startswith(www_path):
                control.page(self.xpress_request_path)
            else:
                self.send_error(404)

        elif os.path.isfile(os.path.join(ui_path, self.path[1:])):
            #if ui file request
            ui_file_path = os.path.join(ui_path, self.path[1:])
            normal_path = os.path.normpath(ui_file_path)
            if normal_path.startswith(ui_path):
                self.send_response(200)
                self.end_headers()
                fd = open(os.path.join(ui_path, self.path[1:]), 'rb')
                self.wfile.write(fd.read())
                fd.close()
            else:
                self.send_error(404)

        elif os.path.isdir(self.xpress_request_path):
            normal_path = os.path.normpath(self.xpress_request_path)
            if normal_path.startswith(www_path):
                control.category(self.xpress_request_path)
            else:
                self.send_error(404)

        else:
            self.send_error(404)

if __name__ == "__main__":
    host = "0.0.0.0"
    port = 9090
    server = BaseHTTPServer.HTTPServer((host, port), WebRequestHandler)
    server.serve_forever()
