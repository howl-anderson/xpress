#!/usr/bin/env python
# -*-coding:utf-8-*-

import os
import time
from datetime import timedelta

from flask import Flask
from flask import request
from flask import g
from flask import abort

from flask import render_template

import markdown
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
# TODO
from pyquery import PyQuery


# create application now
app = Flask(__name__)
app.config['SECRET_KEY'] = 'foobarbaz'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(minutes=30)
app.config['REMEMBER_COOKIE_DURATION'] = timedelta(days=30)


class Xpress:
    def __init__(self, www_path):
        self.www_path = www_path

        self.load_config()

    def load_config(self):
        rc_path = os.path.join(self.www_path, '.xpressrc')
        config_fd = open(rc_path, 'r')
        # NOTICE:only support utf8
        config_stream = config_fd.read().decode("utf8")
        config_fd.close()
        config_info = load(config_stream, Loader=Loader)

        g.config = config_info

    def pagination(self, item_count):
        # TODO: need be configurable
        page_size = 5

        page_num = int((item_count + page_size - 1) / page_size)
        if request.args.get("page") is None:
            page_param = 0
        else:
            page_param = int(request.args.get("page"))
        if page_param < page_num - 1:
            page_start_point = page_param * page_size
            page_end_point = (page_param + 1) * page_size - 1
        elif page_param == page_num - 1:
            page_start_point = page_param * page_size
            page_end_point = item_count - 1
        else:
            abort(500)
        if page_param == 0:
            pre_page_flag = 0
            pre_page = 0
        else:
            pre_page_flag = 1
            pre_page = page_param - 1
        if page_param == page_num - 1:
            next_page_flag = 0
            next_page = 0
        else:
            next_page_flag = 1
            next_page = page_param + 1

        page = dict()
        page["page_start_point"] = page_start_point
        page["page_end_point"] = page_end_point
        page["pre_page_flag"] = pre_page_flag
        page["pre_page"] = pre_page
        page["next_page_flag"] = next_page_flag
        page["next_page"] = next_page
        return page

    def filter_path(self, path):
        """ filter the path if it is not so good """
        # TODO for more secure
        path_list = path.split("/")
        for partial_path in path_list:
            if partial_path.startswith(".") or partial_path.startswith("#"):
                return False
        return True

    def category(self, category_dir):
        file_list = self.scan_dir(category_dir)
        file_sorted_list = self.sort_file_by_ctime(file_list)
        relative_path_list = []
        for file_name in file_sorted_list:
            file_relative_path = os.path.relpath(file_name, category_dir)
            relative_path_list.append(file_relative_path)
        list_length = len(relative_path_list)
        category_path = category_dir

        page = self.pagination(list_length)

        page_content = self.category_page(relative_path_list, category_path, page)
        return page_content

    def category_page(self, file_list, category_path, page):
        if page["page_start_point"] >= len(file_list) or page["page_end_point"] >= len(file_list):
            abort(500)

        index_info = []
        for file_index in range(page["page_start_point"], page["page_end_point"] + 1):
            file_name = file_list[file_index]
            article_file = os.path.join(category_path, file_name)

            article_title, article_content, file_ctime = self.read_article(article_file)
            url = '/' + os.path.relpath(article_file, self.www_path)
            try:
                pyquery_obj = PyQuery(article_content)
                summary_text = pyquery_obj("p:first").html()
                # in case there is no <p>
                if summary_text is None:
                    summary_text = ""
            except Exception,e:
                # TODO:pyquery sometime crash, need do something
                summary_text = ""
            article_summary = "<p>" + summary_text + "</p>"
            article_info = {"url": url,
                            "article": article_content,
                            "title": article_title,
                            "summary": article_summary,
                            "ctime": file_ctime}
            index_info.append(article_info)

        html = render_template('category.html', index=index_info, page=page)
        return html

    def scan_dir(self, directory):
        file_list = []
        yid = os.walk(directory)
        for rootDir, pathList, fileList in yid:
            for file_name in fileList:
                # filter the vi and emacs swap file, other editor? sorry, we only support vi or emacs currently
                # find some better way
                if self.filter_path(rootDir) and self.filter_path(file_name):
                    file_list.append(os.path.join(rootDir, file_name))
        return file_list

    def sort_file_by_ctime(self, file_list, sort_reverse=True):
        file_dict = {}
        for file_name in file_list:
            file_stat = os.stat(file_name)
            file_dict[file_name] = file_stat.st_ctime
        file_sorted_list = sorted(file_dict, key=file_dict.__getitem__, reverse=sort_reverse)
        return file_sorted_list

    def read_article(self, article_path):
        # TODO: meta message will split to other configure file with article name
        file_stat = os.stat(article_path)
        file_ctime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(file_stat.st_ctime))

        fd = open(article_path, 'r')
        content = fd.read().decode("utf8")
        fd.close()
        content_unit = content.split('\n\n', 1)
        article_title = content_unit[0]
        article_title = article_title.split('\n', 1)
        article_title = article_title[0]
        article_content = markdown.markdown(content_unit[1])
        return article_title, article_content, file_ctime

    def page(self, article_file):
        article_title, article_content, file_ctime = self.read_article(article_file)
        html = render_template('content.html', article_content=article_content, article_title=article_title, ctime=file_ctime)
        return html

    def search_page_file(self, request_path, return_bool=False):
        # TODO: to be configurable
        accepted_format_list = (".md", ".markdown")
        potential_request_path = [request_path + page_format for page_format in accepted_format_list]
        for file_name in potential_request_path:
            if os.path.exists(file_name) and os.path.isfile(file_name):
                if return_bool:
                    return True
                else:
                    return file_name
        return False

@app.route('/')
@app.route('/<path:url_path>')
def dynamic(url_path="/"):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    www_path = os.path.join(current_dir, 'www')

    control = Xpress(www_path)

    if request.path == "/":
        request_path = os.path.join(www_path, "")
        return control.category(request_path)
    else:
        # TODO: need some secure filter
        request_path = os.path.join(www_path, url_path)

        if os.path.isdir(request_path):
            return control.category(request_path)
        elif os.path.isfile(request_path):
            return control.page(request_path)
        elif control.search_page_file(request_path, return_bool=True):
            file_path = control.search_page_file(request_path)
            return control.page(file_path)
        else:
            abort(404)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0',port=5000)